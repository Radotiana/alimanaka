package com.example.alimanaka.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.alimanaka.R;

public class AlimanakaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alimanaka);

        TextView janvier = (TextView) findViewById(R.id.Janvier);
        janvier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), AlimanakaResultActivity.class);
                intent.putExtra("mois", "Janvier");
                startActivity(intent);
            }
        });

        TextView fevrier = (TextView) findViewById(R.id.Fevrier);
        fevrier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), AlimanakaResultActivity.class);
                intent.putExtra("mois", "Fevrier");
                startActivity(intent);
            }
        });

        TextView mars = (TextView) findViewById(R.id.Mars);
        mars.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), AlimanakaResultActivity.class);
                intent.putExtra("mois", "Mars");
                startActivity(intent);
            }
        });



        TextView avril = (TextView) findViewById(R.id.Avril);
        avril.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), AlimanakaResultActivity.class);
                intent.putExtra("mois", "Avril");
                startActivity(intent);
            }
        });


        TextView mai = (TextView) findViewById(R.id.Mai);
        mai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), AlimanakaResultActivity.class);
                intent.putExtra("mois", "Mai");
                startActivity(intent);
            }
        });


        TextView juin = (TextView) findViewById(R.id.Juin);
        juin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), AlimanakaResultActivity.class);
                intent.putExtra("mois", "Juin");
                startActivity(intent);
            }
        });


        TextView juillet = (TextView) findViewById(R.id.Juillet);
        juillet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), AlimanakaResultActivity.class);
                intent.putExtra("mois", "Juillet");
                startActivity(intent);
            }
        });


        TextView aout = (TextView) findViewById(R.id.Aout);
        aout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), AlimanakaResultActivity.class);
                intent.putExtra("mois", "Aout");
                startActivity(intent);
            }
        });

        TextView septembre = (TextView) findViewById(R.id.Septembre);
        septembre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), AlimanakaResultActivity.class);
                intent.putExtra("mois", "Septembre");
                startActivity(intent);
            }
        });


        TextView octobre = (TextView) findViewById(R.id.Octobre);
        octobre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), AlimanakaResultActivity.class);
                intent.putExtra("mois", "Octobre");
                startActivity(intent);
            }
        });

        TextView novembre = (TextView) findViewById(R.id.Novembre);
        novembre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), AlimanakaResultActivity.class);
                intent.putExtra("mois", "Novembre");
                startActivity(intent);
            }
        });


        TextView decembre = (TextView) findViewById(R.id.Decembre);
        decembre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), AlimanakaResultActivity.class);
                intent.putExtra("mois", "Decembre");
                startActivity(intent);
            }
        });
    }
}