package com.example.alimanaka.view;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.widget.TextView;

import com.example.alimanaka.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

public class FanekempinoanaActivity extends AppCompatActivity {
    private ScaleGestureDetector scaleGestureDetector;
    private float scaleFactor = 1.0f;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fanekempinoana);

        Bundle param = this.getIntent().getExtras();
        int id = param.getInt("id");
        TextView result = findViewById(R.id.result);
        try{
            String jsonResult = loadJsonFromAssets("fanekena.json");
            if(!jsonResult.isEmpty()){
                JSONObject obj = new JSONObject(jsonResult);
                JSONArray array = obj.getJSONArray("fanekena");
                String texte = array.getJSONObject(id).get("texte").toString();
                result.setText(texte);
            }
        }
        catch (JSONException e){
            e.printStackTrace();
        }

        scaleGestureDetector = new ScaleGestureDetector(this, new ScaleListener());
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        scaleGestureDetector.onTouchEvent(event);
        return true;
    }
    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            scaleFactor *= detector.getScaleFactor();
            scaleFactor = Math.max(0.1f, Math.min(scaleFactor, 10.0f));
            TextView result = findViewById(R.id.result);
            result.setScaleX(scaleFactor);
            result.setScaleY(scaleFactor);

            return true;
        }
    }
    public String loadJsonFromAssets(String fichier){
        String json = null;
        try{
            InputStream is = getAssets().open(fichier);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        }
        catch (IOException ex){
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}