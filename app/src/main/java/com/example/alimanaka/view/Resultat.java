package com.example.alimanaka.view;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.example.alimanaka.R;

public class Resultat extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultat);

        Bundle param = this.getIntent().getExtras();
        String titre = param.getString("titre");
        String fiangonana = param.getString("fiangonana");
        String leksiona = param.getString("leksiona");
        String epistily = param.getString("epistily");
        String filazantsara = param.getString("filazantsara");
        String toriteny = param.getString("toriteny");

        TextView titres = findViewById(R.id.Titre);
        titres.setText(titre);
        TextView fiangonanas = findViewById(R.id.Fiangonana);
        fiangonanas.setText(fiangonana);
        TextView leksionas = findViewById(R.id.Leksiona);
        leksionas.setText("Leksiona: "+ leksiona);
        TextView epistilys = findViewById(R.id.Epistily);
        epistilys.setText("Epistily: "+epistily);
        TextView filazantsaras = findViewById(R.id.Filazantsara);
        filazantsaras.setText("Filazantsara: "+filazantsara);
        TextView toritenys = findViewById(R.id.Toriteny);
        toritenys.setText("Toriteny: "+toriteny);
    }
}