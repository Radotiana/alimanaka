package com.example.alimanaka.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.alimanaka.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class AlimanakaResultActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alimanaka_result);

        Bundle param = this.getIntent().getExtras();
        String mois = param.getString("mois");
        ArrayList<String> list = new ArrayList<String>();
        ArrayList<String> listComplet = new ArrayList<String>();

        try{
            String titre = null;
            String fiangonana = null;
            String leksiona = null;
            String epistily = null;
            String filazantsara = null;
            String toriteny = null;
            String jsonResult = loadJsonFromAssets("fanekena.json");
            if(!jsonResult.isEmpty()){
                JSONObject obj = new JSONObject(jsonResult);
                JSONArray array = obj.getJSONArray(mois);
                for(int i = 0; i<array.length(); i++){
                    String date = array.getJSONObject(i).get("daty").toString();
                    titre = array.getJSONObject(i).get("titre").toString();
                    fiangonana = array.getJSONObject(i).get("fiangonana").toString();
                    leksiona = array.getJSONObject(i).get("leksiona").toString();
                    epistily = array.getJSONObject(i).get("epistily").toString();
                    filazantsara = array.getJSONObject(i).get("filazantsara").toString();
                    toriteny = array.getJSONObject(i).get("toriteny").toString();

                    String valeur = date+ " - "+ titre;
                    list.add(valeur);
                    listComplet.add(titre+";"+fiangonana+";"+leksiona+";"+epistily+";"+filazantsara+";"+toriteny);
                }

                ListView listView = (ListView) findViewById(R.id.list);


                ArrayAdapter<String> adapter= new ArrayAdapter<String>(this, R.layout.activity_list_view,R.id.label, list);
                listView.setAdapter(new ArrayAdapter<String>(this, R.layout.activity_list_view,R.id.label, list));





                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        Intent intent = new Intent(new Intent(getApplicationContext(), Resultat.class));
                        String valeur = listComplet.get(i);
                        String[] valeurs = valeur.split(";");
                        String finalTitre = valeurs[0];
                        String finalFiangonana = valeurs[1];
                        String finalLeksiona = valeurs[2];
                        String finalEpistily = valeurs[3];
                        String finalFilazantsara = valeurs[4];
                        String finalToriteny = valeurs[5];

                        intent.putExtra("titre", finalTitre);
                        intent.putExtra("fiangonana", finalFiangonana);
                        intent.putExtra("leksiona", finalLeksiona);
                        intent.putExtra("epistily", finalEpistily);
                        intent.putExtra("filazantsara", finalFilazantsara);
                        intent.putExtra("toriteny", finalToriteny);
                        startActivity(intent);
                    }
                });
            }
        }
        catch (JSONException e){
            e.printStackTrace();
        }

    }

    public String loadJsonFromAssets(String fichier){
        String json = null;
        try{
            InputStream is = getAssets().open(fichier);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        }
        catch (IOException ex){
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}