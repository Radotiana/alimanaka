package com.example.alimanaka.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.alimanaka.R;

public class FanekenaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fanekena);

        TextView apostolika = (TextView) findViewById(R.id.apostolika);
        apostolika.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), FanekempinoanaActivity.class);
                intent.putExtra("id", 0);
                startActivity(intent);
            }
        });

        TextView niseana = (TextView) findViewById(R.id.niseana);
        niseana.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), FanekempinoanaActivity.class);
                intent.putExtra("id", 1);
                startActivity(intent);
            }
        });
        TextView atanaziana = (TextView) findViewById(R.id.atanaziana);
        atanaziana.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), FanekempinoanaActivity.class);
                intent.putExtra("id", 2);
                startActivity(intent);
            }
        });
    }
}