package com.example.alimanaka.model;

public class Fanekena {
    private int id;
    private String texte;

    public Fanekena (int id, String texte){
        this.id = id;
        this.texte = texte;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTexte() {
        return texte;
    }

    public void setTexte(String texte) {
        this.texte = texte;
    }
}
